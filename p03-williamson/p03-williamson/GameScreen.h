//
//  GameScreen.h
//  p03-williamson
//
//  Created by Matthew Williamson on 2/9/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameScreen : UIView {
    float dx, dy;
}

@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) IBOutlet UILabel *score;

-(void)createPlayField;

@end
