//
//  main.m
//  p03-williamson
//
//  Created by Matthew Williamson on 2/9/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
